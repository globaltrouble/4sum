#include <iostream>
#include <vector>
#include <cstdint>
#include <algorithm>
#include <random>
#include <cstdlib>
#include <ctime>
#include <thread>
#include <chrono>
#include <unordered_map>
using namespace std::chrono;
using namespace std;

#define MAX_VAL 1000000
#define MAX_SIZE 100


vector<int> getPairs(vector<int>& a, vector<int>& b, int maxVal) {
    vector<int> result;
    for (size_t i = 0; i < a.size(); i++) {
        for (size_t j = 0; j < b.size(); j++) {
            int sum = a[i] + b[j];
            if (sum <= maxVal) {
                result.emplace_back(sum);
            }
        }
    }

    std::sort(result.begin(), result.end());
    return result;
}


uint64_t getPairsCount(vector<int>& left, vector<int>& right, int maxVal) {
    if (left.size() == 0 || right.size() == 0) {
        return 0;
    }

    size_t leftInd = 0;
    size_t rightInd = 0;

    uint64_t count = 0;
    while (leftInd < left.size() && left[leftInd] + right[rightInd] <= maxVal) {
        count++;
        leftInd++;
    }
    if (leftInd == 0) {
        return 0;
    }
    // back to last valid val;
    leftInd--;

    // move to unchecked
    rightInd++;
    while (rightInd < right.size() && left[leftInd] + right[rightInd] <= maxVal) {
        count += leftInd + 1;
        rightInd++;
    }

    while (leftInd > 0 && rightInd < right.size()) {
        leftInd--;
        while (rightInd < right.size() && left[leftInd] + right[rightInd] <= maxVal) {
            count += leftInd + 1;
            rightInd++;
        }
    }

    return count;
}


long getPairsCount(vector<int>& a, vector<int>& b, vector<int>& c, vector<int>& d, int maxSum) {
    int maxLeftVal = maxSum - *std::min_element(c.begin(), c.end()) - *std::min_element(d.begin(), d.end());
    vector<int> leftPairs = getPairs(a, b, maxLeftVal);

    if (leftPairs.size() < 1) {
        return 0;
    }

    int maxRightVal = maxSum - *std::min_element(leftPairs.begin(), leftPairs.end());
    vector<int> rightPairs = getPairs(c, d, maxRightVal);

    if (rightPairs.size() < 1) {
        return 0;
    }

//    std::cerr << "left: " << leftPairs.size() << ", right: " << rightPairs.size() << std::endl;

    return getPairsCount(leftPairs, rightPairs, maxSum);

}

long getNaivePairsCount(vector<int>& a, vector<int>& b, vector<int>& c, vector<int>& d, int maxSum) {
    uint64_t count = 0;
    for (auto i : a) {
        for (auto j : b) {
            for (auto k : c) {
                for (auto m : d) {
                    if (((uint64_t)(i + j)) + ((uint64_t)(k + m)) <= (uint64_t) maxSum) {
                       count++;
                    }
                }
            }
        }
    }

    return count;
}


long getOptPairsCount(vector<int>& a, vector<int>& b, vector<int>& c, vector<int>& d, int64_t maxSum) {
    uint64_t count = 0;
    std::unordered_map<int64_t, int64_t> pairs;
    pairs.reserve(a.size() * b.size());

    for (auto i : a) {
        for (auto j : b) {
            int64_t sum = i + j;

            auto it = pairs.find(sum);
            if (it != pairs.end()) {
                it->second += 1;
            } else {
                pairs.insert({sum, 1});
            }
        }
    }

    for (auto i : c) {
        for (auto j : d) {
            int64_t target = maxSum - i - j;
            auto pair = pairs.find(target);
            if (pair != pairs.end()) {
                count += pair->second;
            }
        }
    }

    return count;
}


vector<int> createRandomVector(size_t maxSize, int maxVal) {
//    size_t size = std::rand() % maxSize + 1;
    int size = maxSize;
    vector<int> result;
    result.reserve(size);
    for (size_t i = 0; i < size; i++) {
        result.emplace_back(std::rand() % maxVal + 1);
    }
    return result;
}


void test(int count) {
    std::hash<std::thread::id> hasher;
    std::srand(std::time(nullptr) + (int)hasher(std::this_thread::get_id())); // use current time as seed for random generator

    while (--count >= 0) {
        vector<int> a = createRandomVector(MAX_SIZE, MAX_VAL);
        std::sort(a.begin(), a.end());

        vector<int> b = createRandomVector(MAX_SIZE, MAX_VAL);
        std::sort(b.begin(), b.end());

        vector<int> c = createRandomVector(MAX_SIZE, MAX_VAL);
        std::sort(c.begin(), c.end());

        vector<int> d = createRandomVector(MAX_SIZE, MAX_VAL);
        std::sort(d.begin(), d.end());

        int maxVal = std::rand() % MAX_VAL + 1;
        uint64_t naive = getNaivePairsCount(a, b, c, d, maxVal);
//        uint64_t res = getOptPairsCount(a, b, c, d, maxVal);
        uint64_t res = getPairsCount(a, b, c, d, maxVal);

        std::cout << "Naive: " << naive << ", res: " << res << std::endl;

        if (naive != res) {
            std::cerr << "==============" << std::endl;
            std::cerr << "NAIVE: " << naive << ", RES: " << res << ", maxVal: " << maxVal << std::endl;
            std::cerr << "a: ";
            for (auto i : a) {
                std::cerr << i << ',';
            }
            std::cerr << std::endl;

            std::cerr << "b: ";
            for (auto i : b) {
                std::cerr << i << ',';
            }
            std::cerr << std::endl;

            std::cerr << "c: ";
            for (auto i : c) {
                std::cerr << i << ',';
            }
            std::cerr << std::endl;

            std::cerr << "d: ";
            for (auto i : d) {
                std::cerr << i << ',';
            }
            std::cerr << std::endl;

            std::cerr << "==============" << std::endl;
        }
    }

}



int main()
{
//    vector<int> a{2, 3};
//    vector<int> b{4};
//    vector<int> c{2, 3};
//    vector<int> d{1, 2};
//    int maxVal = 10;

    std::hash<std::thread::id> hasher;
    std::srand(std::time(nullptr) + (int)hasher(std::this_thread::get_id())); // use current time as seed for random generator

    int maxVal = 1000000000;
    vector<int> a = createRandomVector(1000, 1000000000);
    vector<int> b = createRandomVector(1000, 1000000000);
    vector<int> c = createRandomVector(1000, 1000000000);
    vector<int> d = createRandomVector(1000, 1000000000);

    auto start = high_resolution_clock::now();
    auto res = getPairsCount(a, b, c, d, maxVal);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);
    cout << "RES: " << res << ", DUR: " << duration.count() << endl;

    test(100);

    return 0;
}
